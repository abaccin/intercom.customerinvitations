﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Intercom.CustomerInvitations.Services.Configuration
{
    public class InputOutputConfiguration
    {
        public string InputFile { get; set; }

        public string OutputFile { get; set; }
    }
}
