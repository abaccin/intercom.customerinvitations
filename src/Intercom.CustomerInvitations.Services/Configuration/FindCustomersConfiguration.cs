﻿using Intercom.CustomerInvitations.Models;
using Intercom.CustomerInvitations.Models.ValueObjects;

namespace Intercom.CustomerInvitations.Services.Configuration
{
	public class FindCustomersConfiguration
	{
		public double Distance { get; set; } = 100;
		public DistanceMeasurementUnit Unit { get; set; } = DistanceMeasurementUnit.Km;
		public Angle OfficeLongitude { get; set; } = new Angle(-6.257664, AngleMeasurementUnit.Degrees);
		public Angle OfficeLatitude { get; set; } = new Angle(53.339428, AngleMeasurementUnit.Degrees);
	}
}
