﻿using System.Linq;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Models.ValueObjects;
using Intercom.CustomerInvitations.Services.Configuration;
using Intercom.CustomerInvitations.Services.Providers;
using Microsoft.Extensions.Logging;

namespace Intercom.CustomerInvitations.Services
{
	public class ExtractCustomersToInviteCommand
	{
		private readonly FindCustomersConfiguration configuration;
		private readonly ICustomerDataSource inputDataSource;
		private readonly IResultOutput destination;
		private readonly ILogger<ExtractCustomersToInviteCommand> logger;

		public ExtractCustomersToInviteCommand(
            FindCustomersConfiguration configuration, 
            ICustomerDataSource inputDataSource,
			IResultOutput destination, ILogger<ExtractCustomersToInviteCommand> logger)
		{
			this.configuration = configuration;
			this.inputDataSource = inputDataSource;
			this.destination = destination;
			this.logger = logger;
		}

		public async Task Execute()
		{
			var maximumDistance = Distance.From(configuration.Distance, configuration.Unit);

			var officeLocation = new Location(configuration.OfficeLatitude, configuration.OfficeLongitude);

            var result = inputDataSource.Customers
                .Where(x => officeLocation.DistanceTo(x.Latitude, x.Longitude) < maximumDistance)
                .OrderBy(x => x.UserId);

            await destination.WriteOutput(result).ConfigureAwait(false);

			logger.LogInformation("Finished Processing!");
		}
	}
}
