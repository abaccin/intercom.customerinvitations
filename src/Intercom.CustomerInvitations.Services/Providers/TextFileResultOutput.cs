﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Models.Model;
using Intercom.CustomerInvitations.Services.Configuration;
using Microsoft.Extensions.Logging;

namespace Intercom.CustomerInvitations.Services.Providers.Text
{
	public class TextFileResultOutput : IResultOutput
	{
        private readonly InputOutputConfiguration inputOutputConfiguration;
        private readonly ILogger<TextFileResultOutput> logger;

		public TextFileResultOutput(InputOutputConfiguration inputOutputConfiguration, ILogger<TextFileResultOutput> logger)
		{
            this.inputOutputConfiguration = inputOutputConfiguration;
            this.logger = logger;
		}

		public async Task WriteOutput(IEnumerable<Customer> customers)
		{
			await using var file = File.Open(inputOutputConfiguration.OutputFile, FileMode.Create, FileAccess.Write);
			await using var writer = new StreamWriter(file);
			foreach (var customer in customers)
			{
				await writer.WriteLineAsync($"{customer.UserId} {customer.Name}").ConfigureAwait(false);
			}
		}
	}
}
