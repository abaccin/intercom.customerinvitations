﻿using System;
using System.Collections.Generic;
using Intercom.CustomerInvitations.Models.Model;

namespace Intercom.CustomerInvitations.Services.Providers
{
	public interface ICustomerDataSource
	{
		IReadOnlyList<Customer> Customers { get; }
	}
}
