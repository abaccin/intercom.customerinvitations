﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Models.Model;

namespace Intercom.CustomerInvitations.Services.Providers
{
	public interface IResultOutput
	{
		Task WriteOutput(IEnumerable<Customer> customers);
	}
}
