﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Models.Model;
using Intercom.CustomerInvitations.Services.Configuration;
using Newtonsoft.Json;

namespace Intercom.CustomerInvitations.Services.Providers.Json
{
    public class JsonFileCustomerDataSource : ICustomerDataSource
    {
        private readonly InputOutputConfiguration inputOutputConfiguration;

        public JsonFileCustomerDataSource(InputOutputConfiguration inputOutputConfiguration)
        {
            this.inputOutputConfiguration = inputOutputConfiguration;
            Customers = LoadCustomersFromFile(inputOutputConfiguration.InputFile);
        }


        private List<Customer> LoadCustomersFromFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return new List<Customer>();
            }
            var res = new List<Customer>();
            using var reader = new StreamReader(File.OpenRead(filePath));
            while (!reader.EndOfStream)
            {
                res.Add(JsonConvert.DeserializeObject<Customer>(reader.ReadLine() ?? string.Empty));
            }

            return res;
        }

        public IReadOnlyList<Customer> Customers { get; }
    }
}
