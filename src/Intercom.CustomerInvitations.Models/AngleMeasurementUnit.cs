﻿namespace Intercom.CustomerInvitations.Models
{
	public enum AngleMeasurementUnit
	{
		Degrees,
		Radians
	}
}
