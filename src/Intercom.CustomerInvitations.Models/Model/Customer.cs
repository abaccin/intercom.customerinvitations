﻿using Newtonsoft.Json;

namespace Intercom.CustomerInvitations.Models.Model
{
	public class Customer
	{

		[JsonProperty("latitude")]
		public double Latitude { get; set; }

		[JsonProperty("user_id")]
		public long UserId { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("longitude")]
		public double Longitude { get; set; }
	}
}
