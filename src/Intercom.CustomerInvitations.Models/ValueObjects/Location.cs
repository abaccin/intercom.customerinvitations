﻿using System;
using CSharpFunctionalExtensions;

namespace Intercom.CustomerInvitations.Models.ValueObjects
{
	public class Location : ValueObject<Location>
	{

		public Angle Latitude { get; }

		public Angle Longitude { get; }

		public Location(Angle latitude, Angle longitude)
		{
			Latitude = latitude;
			Longitude = longitude;
		}

		protected override bool EqualsCore(Location other) =>
			Math.Abs(other.Latitude - Latitude) < Tolerance &&
			Math.Abs(other.Longitude - Longitude) < Tolerance;

		protected override int GetHashCodeCore()
		{
			var hashCode = base.GetHashCode();
			hashCode = (hashCode * 397) ^ Latitude.GetHashCode();
			hashCode = (hashCode * 397) ^ Longitude.GetHashCode();
			return hashCode;
		}

		public Distance DistanceTo(double lat, double lon) =>
			Distance.Between(this, new Location(new Angle(lat, AngleMeasurementUnit.Degrees), new Angle(lon, AngleMeasurementUnit.Degrees)));

		private const double Tolerance = 0.000000000000000001;

	}
}
