﻿using System;
using CSharpFunctionalExtensions;

namespace Intercom.CustomerInvitations.Models.ValueObjects
{
	public class Angle : ValueObject<Angle>
	{

		public double Value { get; }

		public Angle(double angle, AngleMeasurementUnit unit)
		{
			Value = unit switch
			{
				AngleMeasurementUnit.Degrees => angle * Math.PI / 180,
				AngleMeasurementUnit.Radians => angle,
				_ => throw new ArgumentOutOfRangeException(nameof(unit), unit, null)
			};
		}
		protected override bool EqualsCore(Angle other) =>
			Math.Abs(other.Value - Value) < Tolerance;

		protected override int GetHashCodeCore()
			=> (base.GetHashCode() * 397) ^ Value.GetHashCode();

		public static bool operator <(Angle a, Angle b)
			=> a.Value < b.Value;

		public static bool operator >(Angle a, Angle b)
			=> a.Value > b.Value;

		public static double operator +(Angle a, Angle b)
			=> a.Value + b.Value;

		public static double operator -(Angle a, Angle b)
			=> a.Value - b.Value;

		private const double Tolerance = 0.000000000000000001;
	}
}
