﻿using System;
using CSharpFunctionalExtensions;

namespace Intercom.CustomerInvitations.Models.ValueObjects
{
	public class Distance : ValueObject<Distance>
	{
		private const double EarthRadius = 6371008.7714;
		private readonly double valueInMeters;

		private Distance(double valueInMeters)
		{
			this.valueInMeters = valueInMeters;
		}

		public static Distance From(double value, DistanceMeasurementUnit unit) =>
			unit switch
			{
				DistanceMeasurementUnit.Mt => new Distance(value),
				DistanceMeasurementUnit.Km => new Distance(1000 * value),
				_ => throw new ArgumentOutOfRangeException(nameof(unit), unit, null)
			};

		public static Distance Between(Location loc1, Location loc2)
		{

			var deltaSigma = Math.Acos(
                Math.Sin(loc1.Latitude.Value) * Math.Sin(loc2.Latitude.Value) +
                Math.Cos(loc1.Latitude.Value) * Math.Cos(loc2.Latitude.Value) * Math.Cos(loc1.Longitude - loc2.Longitude)
                );

			return 
                From(deltaSigma * EarthRadius, DistanceMeasurementUnit.Mt);
		}

		public static bool operator <(Distance a, Distance b)
			=> a.valueInMeters < b.valueInMeters;

		public static bool operator >(Distance a, Distance b)
			=> b < a;

		protected override bool EqualsCore(Distance other) =>
			Math.Abs(other.valueInMeters - valueInMeters) < Tolerance;

		protected override int GetHashCodeCore()
			=> (base.GetHashCode() * 397) ^ valueInMeters.GetHashCode();

		private const double Tolerance = 0.000000000000000001;


	}
}
