﻿namespace Intercom.CustomerInvitations.Models
{
	public enum DistanceMeasurementUnit
	{
		Mt,
		Km,
	}
}
