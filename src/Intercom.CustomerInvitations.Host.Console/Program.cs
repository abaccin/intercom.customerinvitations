using System.Collections.Generic;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Services;
using Intercom.CustomerInvitations.Services.Configuration;
using Intercom.CustomerInvitations.Services.Providers;
using Intercom.CustomerInvitations.Services.Providers.Json;
using Intercom.CustomerInvitations.Services.Providers.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Intercom.CustomerInvitations.Host.Console
{
    public class Program
    {

        public static async Task Main(string[] args)
        {
            var host = ConfigureApplication(args).Build();

            await host.Services
                .GetRequiredService<ExtractCustomersToInviteCommand>()
                .Execute().ConfigureAwait(false);
        }

        public static IHostBuilder ConfigureApplication(string[] args) =>
            Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder =>
                {
                    builder.Sources.Clear();
                    builder.AddCommandLine(args, new Dictionary<string, string>
                                                 {
                                                     { "-i", "InputFile" },
                                                     { "-o", "OutputFile" },
                                                 });
                })
                .ConfigureLogging((context, builder) =>
                {
                    builder.ClearProviders();
                    builder.AddConsole();
                })
                .ConfigureServices((hostingContext, services) =>
                {
                    var config = hostingContext.Configuration.Get<InputOutputConfiguration>();
                    services.AddSingleton(config);
                    services.AddSingleton<FindCustomersConfiguration>();
                    services.AddSingleton<ExtractCustomersToInviteCommand>();
                    services.AddSingleton<ICustomerDataSource, JsonFileCustomerDataSource>();
                    services.AddSingleton<IResultOutput, TextFileResultOutput>();
                });


    }
}
