## Intercom Code Assignment
The project is based on .net core 3.1 and can run on both windows and linux, alternatively it can run with zero footprint on the host machine using docker.

### .NET Environment
##### Prerequisites
<a href="https://dotnet.microsoft.com/download/dotnet-core/3.1" target="_blank">.NET Core 3.1 DSK</a>

The following commands are to be executed in a terminal of your choice (bash, powershell, etc.)

##### Compilation
```
dotnet restore
dotnet build
```

##### Execution
```
dotnet restore
dotnet build
dotnet run -p src/Intercom.CustomerInvitations.Host.Console/Intercom.CustomerInvitations.Host.Console.csproj -- -i data/customers.txt -o data/output.txt
```

##### Testing
```
dotnet test
```

### Docker Environment
<a href="https://www.docker.com" target="_blank">Docker</a> needs to be installed in the executing host and relies on the docker-compose.yaml file containing the orchestration for the services, in this specific case it will be only one.

```
docker-compose -f docker/docker-compose.yml up
```





