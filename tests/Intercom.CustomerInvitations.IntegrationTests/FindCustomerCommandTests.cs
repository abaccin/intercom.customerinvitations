﻿using System.IO;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Services;
using Intercom.CustomerInvitations.Services.Configuration;
using Intercom.CustomerInvitations.Services.Providers.Json;
using Intercom.CustomerInvitations.Services.Providers.Text;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace Intercom.CustomerInvitations.IntegrationTests
{
    public class FindCustomerCommandTests
    {
        private readonly FindCustomersConfiguration defaultConfiguration = new FindCustomersConfiguration();

        [Fact]
        public async Task Customers_within_100km_should_be_written_to_output()
        {
            var conf = new InputOutputConfiguration
                           {
                               InputFile = GetContextFolderForMultiPlatformTarget("data/intercomcustomers.json"),
                               OutputFile = GetContextFolderForMultiPlatformTarget("output.txt")
                           };

            var input = new JsonFileCustomerDataSource(conf);

            var destination = new TextFileResultOutput(conf, new NullLogger<TextFileResultOutput>());

            var subject = new ExtractCustomersToInviteCommand(defaultConfiguration, 
                input,
                destination, 
                new NullLogger<ExtractCustomersToInviteCommand>());

            await subject.Execute().ConfigureAwait(false);

            Assert.Equal(LineUtility.CombineWithLineBreak(
                    "4 Ian Kehoe",
                    "5 Nora Dempsey",
                    "6 Theresa Enright",
                    "8 Eoin Ahearn",
                    "11 Richard Finnegan",
                    "12 Christina McArdle",
                    "13 Olive Ahearn",
                    "15 Michael Ahearn",
                    "17 Patricia Cahill",
                    "23 Eoin Gallagher",
                    "24 Rose Enright",
                    "26 Stephen McArdle",
                    "29 Oliver Ahearn",
                    "30 Nick Enright",
                    "31 Alan Behan",
                    "39 Lisa Ahearn"),
                File.ReadAllText(GetContextFolderForMultiPlatformTarget("output.txt")));
        }



        private static string GetContextFolderForMultiPlatformTarget(string relativeTestFile) =>
            Path.Combine(relativeTestFile, Path.GetDirectoryName(typeof(FindCustomerCommandTests).Assembly.FullName));

    }
}
