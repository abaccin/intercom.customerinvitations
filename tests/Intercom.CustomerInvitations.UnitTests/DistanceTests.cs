﻿using Intercom.CustomerInvitations.Models;
using Intercom.CustomerInvitations.Models.ValueObjects;
using Xunit;

namespace Intercom.CustomerInvitations.UnitTests
{
    public class DistanceTests : TestBase
    {
        private static readonly Location MyHome = new Location(new Angle(53.285194, AngleMeasurementUnit.Degrees), new Angle(-6.431944, AngleMeasurementUnit.Degrees));
        private static readonly Location Treviso = new Location(new Angle(45.665452, AngleMeasurementUnit.Degrees), new Angle(12.246390, AngleMeasurementUnit.Degrees));


        [Fact]
        public void Distance_created_from_different_units_are_the_same()
        {
            //Arrange
            double val = 54325345;
            //Act
            var distanceA = Distance.From(val, DistanceMeasurementUnit.Km);
            var distanceB = Distance.From(val * 1000, DistanceMeasurementUnit.Mt);

            //Assert
            Assert.Equal(distanceB, distanceA);
        }


        [Fact]
        public void Distance_between_myhouse_and_Treviso_is_valid()
        {
            //Arrange
            var res = Distance.From(1586225.815205469, DistanceMeasurementUnit.Mt);

            //Act
            var distance = Distance.Between(MyHome, Treviso);

            //Assert
            Assert.Equal(res, distance);
        }
    }
}
