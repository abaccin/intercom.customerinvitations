﻿using System.Linq;
using Intercom.CustomerInvitations.Services.Configuration;
using Intercom.CustomerInvitations.Services.Providers.Json;
using Xunit;

namespace Intercom.CustomerInvitations.UnitTests
{
    public class JsonFileCustomerProviderTests : TestBase
    {

        private readonly JsonFileCustomerDataSource sut;

        public JsonFileCustomerProviderTests()
        {
            sut = new JsonFileCustomerDataSource(new InputOutputConfiguration { InputFile = GetContextFolderForMultiPlatformTarget("data/intercomcustomers.json") });

        }
        [Fact]
        public void JsonFileCustomerDataSource_loads_all_items_from_file()
        {

            //Arrange

            //Act

            //Assert
            Assert.Equal(32, sut.Customers.Count);
        }


        [Fact]
        public void JsonFileCustomerDataSource_check_for_specific_name()
        {

            //Arrange

            //Act
            var specificName = sut.Customers.Where(x => x.Name.Contains("Eoin"));

            //Assert
            Assert.Equal(2, specificName.Count());
        }


    }
}
