﻿using System.IO;

namespace Intercom.CustomerInvitations.UnitTests
{
    public class TestBase
    {
        internal string GetContextFolderForMultiPlatformTarget(string relativeTestFile) =>
            Path.Combine(relativeTestFile, Path.GetDirectoryName(typeof(TestBase).Assembly.FullName) ?? string.Empty);
    }
}