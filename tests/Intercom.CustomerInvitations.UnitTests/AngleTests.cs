﻿using Intercom.CustomerInvitations.Models;
using Intercom.CustomerInvitations.Models.ValueObjects;
using Xunit;

namespace Intercom.CustomerInvitations.UnitTests
{
	public class AngleTests: TestBase
	{
	
		[Fact]
		public void Angle_created_in_degrees_radians_value_correct()
		{
			//Arrange
            var sut = new Angle(53.285184, AngleMeasurementUnit.Degrees);

            //Act

            //Assert
            Assert.Equal(0.9300019, sut.Value, 5 );
		}

        [Fact]
        public void Angle_created_in_radians_value_correct()
        {
            //Arrange
            var sut = new Angle(0.9300019, AngleMeasurementUnit.Radians);

            //Act

            //Assert
            Assert.Equal(0.9300019, sut.Value, 5);
        }
	}
}
