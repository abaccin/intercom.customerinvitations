﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Intercom.CustomerInvitations.Models.Model;
using Intercom.CustomerInvitations.Services.Configuration;
using Intercom.CustomerInvitations.Services.Providers.Text;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace Intercom.CustomerInvitations.UnitTests
{
    public class TextCustomerDestinationTests : TestBase
    {

        [Fact]
        public async Task Result_file_has_correct_entries_in_the_right_format()
        {
            //Arrange
            var subject = new TextFileResultOutput(new InputOutputConfiguration { OutputFile = GetContextFolderForMultiPlatformTarget("output.txt") }, new NullLogger<TextFileResultOutput>());

            //Act
            await subject.WriteOutput(new[]
            {
                new Customer { UserId= 1, Name  = "Leonard Nimoy", Latitude = 1.1, Longitude = 2.2},
                new Customer { UserId= 2, Name  = "William Shatner", Latitude = 3.3, Longitude = 4.5},
            }).ConfigureAwait(false);

            var res = await File.ReadAllLinesAsync(GetContextFolderForMultiPlatformTarget("output.txt")).ConfigureAwait(false);

            //Assert
            Assert.True(res.SequenceEqual(new List<string>() { "1 Leonard Nimoy", "2 William Shatner" }));
        }

    }
}
